package com.gitlab.spring.cloud.rest.cloudfoundry;

/*
 * #%L
 * spring-cloud-rest-cloudfoundry-connector
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.gitlab.spring.cloud.rest.service.RestServiceInfo;
import org.springframework.cloud.cloudfoundry.CloudFoundryServiceInfoCreator;
import org.springframework.cloud.cloudfoundry.Tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This is the cloud foundry version of the {@link org.springframework.cloud.ServiceInfoCreator}.
 *
 * {@inheritDoc}
 *
 * @author Rolf Aden
 */
public class RestServiceInfoCreator extends CloudFoundryServiceInfoCreator<RestServiceInfo> {

    public RestServiceInfoCreator() {
        super(new Tags(), "rest", "rests");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RestServiceInfo createServiceInfo(Map<String, Object> serviceData) {
        final String id = (String) serviceData.get("name");

        final Map<String, Object> credentials = getCredentials(serviceData);
        final String uri = getUriFromCredentials(credentials);

        final RestServiceInfo serviceInfo = new RestServiceInfo(id, uri);
        serviceInfo.setAccessTokenUri(getStringFromCredentials(credentials, "accessTokenUri"));
        serviceInfo.setClientId(getStringFromCredentials(credentials, "clientId"));
        serviceInfo.setClientSecret(getStringFromCredentials(credentials, "clientSecret"));
        serviceInfo.setScopes(toList(getStringFromCredentials(credentials, "scopes")));
        serviceInfo.setUserName(getStringFromCredentials(credentials, "username"));
        serviceInfo.setPassword(getStringFromCredentials(credentials, "password"));
        return serviceInfo;
    }

    private List<String> toList(final String scopes) {
        return scopes == null ? new ArrayList<>() : Arrays.asList(scopes.split(","));
    }

}
