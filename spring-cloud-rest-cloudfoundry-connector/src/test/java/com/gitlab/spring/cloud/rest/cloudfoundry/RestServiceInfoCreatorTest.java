package com.gitlab.spring.cloud.rest.cloudfoundry;

/*
 * #%L
 * spring-cloud-rest-cloudfoundry-connector
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.gitlab.spring.cloud.rest.service.RestServiceConnectorCreator;
import com.gitlab.spring.cloud.rest.service.RestServiceInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.cloud.cloudfoundry.CloudFoundryServiceInfoCreator;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.collection.IsArrayContainingInAnyOrder.arrayContainingInAnyOrder;
import static org.junit.Assert.assertThat;

/**
 * Tests the {@link RestServiceConnectorCreator}
 *
 * @author Rolf Aden
 */
@RunWith(MockitoJUnitRunner.class)
public class RestServiceInfoCreatorTest {

    @Spy
    private Handler handler;

    @Before
    public void setupLogger() {
        final Logger logger = Logger.getLogger(RestServiceConnectorCreator.class.getName());
        handler.setLevel(Level.ALL);
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.ALL);
    }

    @After
    public void cleanupLogger() {
        final Logger logger = Logger.getLogger(RestServiceConnectorCreator.class.getName());
        logger.removeHandler(handler);
    }

    @Test
    public void shouldLogMissingRestServiceConfigYAML() {
        // Given
        final Map<String, Object> serviceData = new HashMap<>();
        serviceData.put("name", "my-rest-service");
        final Map<String, Object> credentials = new HashMap<>();
        credentials.put("uri", "http://host.example.org");
        credentials.put("accessTokenUri", "http://localhost:8080/oauth");
        credentials.put("clientId", "my-client-id");
        credentials.put("clientSecret", "my-client-secret");
        credentials.put("scopes", "read,write");
        credentials.put("username", "me");
        credentials.put("password", "pa$$word");
        serviceData.put("credentials", credentials);

        final RestServiceInfoCreator serviceInfoCreator = new RestServiceInfoCreator();

        // When
        final RestServiceInfo serviceInfo = serviceInfoCreator.createServiceInfo(serviceData);

        // Then
        assertThat(serviceInfo.getId(), is("my-rest-service"));
        assertThat(serviceInfo.getUri(), is("http://host.example.org"));
        assertThat(serviceInfo.getAccessTokenUri(), is("http://localhost:8080/oauth"));
        assertThat(serviceInfo.getClientId(), is("my-client-id"));
        assertThat(serviceInfo.getClientSecret(), is("my-client-secret"));
        assertThat(serviceInfo.getScopes().toArray(), arrayContainingInAnyOrder("read", "write"));
        assertThat(serviceInfo.getUserName(), is("me"));
        assertThat(serviceInfo.getPassword(), is("pa$$word"));
    }

    @Test
    public void shouldLoadService() {
        final ServiceLoader<CloudFoundryServiceInfoCreator> serviceInfoCreators = ServiceLoader.load(CloudFoundryServiceInfoCreator.class);

        assertThat(serviceInfoCreators, hasItem(instanceOf(RestServiceInfoCreator.class)));
    }

}
