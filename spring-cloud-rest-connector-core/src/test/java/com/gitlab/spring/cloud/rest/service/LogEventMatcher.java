package com.gitlab.spring.cloud.rest.service;

/*
 * #%L
 * spring-cloud-rest-connector-core
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.mockito.ArgumentMatcher;

import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * {@link ArgumentMatcher} for {@link LogRecord}
 *
 * TODO deduplicate
 *
 * @author Rolf Aden
 */
public class LogEventMatcher extends ArgumentMatcher<LogRecord> {

    private final Level level;
    private final String messageStartsWith;

    public LogEventMatcher(Level level, String messageStartsWith) {
        this.level = level;
        this.messageStartsWith = messageStartsWith;
    }

    @Override
    public boolean matches(Object argument) {
        if (!(argument instanceof LogRecord)) {
            return false;
        }

        final LogRecord logRecord = (LogRecord) argument;
        return level == logRecord.getLevel() && logRecord.getMessage().startsWith(messageStartsWith);
    }

}
