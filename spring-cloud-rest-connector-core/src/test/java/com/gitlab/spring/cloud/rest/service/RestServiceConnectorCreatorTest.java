package com.gitlab.spring.cloud.rest.service;

/*
 * #%L
 * spring-cloud-rest-connector-core
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.cloud.service.ServiceConnectorConfig;
import org.springframework.cloud.service.ServiceConnectorCreator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.ServiceLoader;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.gitlab.spring.cloud.rest.service.RestServiceConnectorConfigBuilder.newRestServiceConnectorConfig;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

/**
 * Tests the {@link RestServiceConnectorCreator}
 *
 * @author Rolf Aden
 */
@RunWith(MockitoJUnitRunner.class)
public class RestServiceConnectorCreatorTest {

    public static final String START_OF_MESSAGE_WRONG_CONFIG_CLASS = "Use class com.gitlab.spring.cloud.rest.service.RestServiceConnectorConfig for configuring the rest service connection";
    public static final LogEventMatcher LOG_EVENT_MATCHER_WRONG_CONFIG_CLASS = new LogEventMatcher(Level.WARNING, START_OF_MESSAGE_WRONG_CONFIG_CLASS);
    public static final String START_OF_MESSAGE_REST_WITH_BASIC_AUTH = "You're using 'rest' scheme for basic authentication, better use 'rests'";
    public static final LogEventMatcher LOG_EVENT_MATCHER_REST_WITH_BASIC_AUTH = new LogEventMatcher(Level.WARNING, START_OF_MESSAGE_REST_WITH_BASIC_AUTH);

    public static final String HOST = "example.com";
    public static final int PORT = 8080;

    public static final String RESTS_BASE_URI = String.format("rests://%s:%d", HOST, PORT);
    public static final String REST_BASE_URI = String.format("rest://%s:%d", HOST, PORT);
    public static final String PATH = "/path";

    @Spy
    private Handler handler;

    @Before
    public void setupLogger() {
        final Logger logger = Logger.getLogger(RestServiceConnectorCreator.class.getName());
        handler.setLevel(Level.ALL);
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.ALL);
    }

    @After
    public void cleanupLogger() {
        final Logger logger = Logger.getLogger(RestServiceConnectorCreator.class.getName());
        logger.removeHandler(handler);
    }

    @Test
    public void shouldComplainAboutWrongServiceConnectorConfig() {
        // Given
        final RestServiceConnectorCreator connectorCreator = new RestServiceConnectorCreator();
        final ServiceConnectorConfig connectorConfig = new ServiceConnectorConfig() {
        };
        final RestServiceInfo serviceInfo = new RestServiceInfo("id", RESTS_BASE_URI);

        // When
        final RestTemplate restTemplate = connectorCreator.create(serviceInfo, connectorConfig);

        // Then
        assertRestTemplate(restTemplate, "https");
        assertThat(restTemplate.getInterceptors(), empty());
        verify(handler, times(1)).publish(argThat(LOG_EVENT_MATCHER_WRONG_CONFIG_CLASS));
    }

    @Test
    public void shouldReturnTheRestTemplate() throws URISyntaxException, IOException {
        final SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = spy(new SimpleClientHttpRequestFactory());
        // Given
        final RestServiceConnectorCreator connectorCreator = new RestServiceConnectorCreator() {
            @Override
            SimpleClientHttpRequestFactory newSimpleClientHttpRequestFactory() {
                return simpleClientHttpRequestFactory;
            }
        };
        final ServiceConnectorConfig connectorConfig = newRestServiceConnectorConfig()
                .withConnectTimeout(1000).withReadTimeout(2000).build();
        final RestServiceInfo serviceInfo = new RestServiceInfo("id", REST_BASE_URI);

        // When
        final RestTemplate restTemplate = connectorCreator.create(serviceInfo, connectorConfig);

        // Then
        assertThat(restTemplate, instanceOf(RestTemplate.class));
        assertRestTemplate(restTemplate, "http");
        assertThat(restTemplate.getInterceptors(), empty());
        Mockito.verify(simpleClientHttpRequestFactory).setConnectTimeout(1000);
        Mockito.verify(simpleClientHttpRequestFactory).setReadTimeout(2000);

        verify(handler, never()).publish(argThat(LOG_EVENT_MATCHER_WRONG_CONFIG_CLASS));
    }

    @Test
    public void shouldReturnAnOAuth2RestTemplate() {
        // Given
        final DefaultOAuth2ClientContext oAuth2ClientContext = new DefaultOAuth2ClientContext(new DefaultAccessTokenRequest());
        final RestServiceConnectorConfig connectorConfig = newRestServiceConnectorConfig()
                .withOAuth2ClientContext(oAuth2ClientContext).build();
        final RestServiceInfo serviceInfo = new RestServiceInfo("id", RESTS_BASE_URI);
        serviceInfo.setAccessTokenUri("http://example.com/oauth2");
        serviceInfo.setClientId("my-client-id");
        serviceInfo.setClientSecret("my-client-secret");
        serviceInfo.setScopes(Arrays.asList("scope1", "scope2"));

        // When
        final RestServiceConnectorCreator connectorCreator = new RestServiceConnectorCreator();
        final RestTemplate restTemplate = connectorCreator.create(serviceInfo, connectorConfig);

        // Then
        assertRestTemplate(restTemplate, "https");
        assertThat(restTemplate, instanceOf(OAuth2RestTemplate.class));
        assertThat(restTemplate.getInterceptors(), empty());

        final OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) restTemplate;
        assertThat(oAuth2RestTemplate.getOAuth2ClientContext(), sameInstance(oAuth2ClientContext));
        assertThat(oAuth2RestTemplate.getResource().getAccessTokenUri(), is("http://example.com/oauth2"));
        assertThat(oAuth2RestTemplate.getResource().getClientId(), is("my-client-id"));
        assertThat(oAuth2RestTemplate.getResource().getClientSecret(), is("my-client-secret"));
        assertThat(oAuth2RestTemplate.getResource().getScope(), containsInAnyOrder("scope1", "scope2"));
    }

    @Test
    public void shouldReturnOAuth2RestTemplateWithDefaultOAuth2ClientContext() {
        final ResourceOwnerPasswordResourceDetails resourceDetails = spy(new ResourceOwnerPasswordResourceDetails());

        // Given
        final RestServiceConnectorCreator connectorCreator = new RestServiceConnectorCreator() {
            @Override
            ResourceOwnerPasswordResourceDetails newResourceOwnerPasswordResourceDetails() {
                return resourceDetails;
            }
        };
        final RestServiceConnectorConfig connectorConfig = newRestServiceConnectorConfig().build();
        final RestServiceInfo serviceInfo = new RestServiceInfo("id", RESTS_BASE_URI);
        serviceInfo.setAccessTokenUri("http://example.com/oauth2");
        serviceInfo.setUserName("user1");
        serviceInfo.setPassword("secret1");

        // When
        final RestTemplate restTemplate = connectorCreator.create(serviceInfo, connectorConfig);

        // Then
        assertRestTemplate(restTemplate, "https");
        assertThat(restTemplate, instanceOf(OAuth2RestTemplate.class));
        assertThat(restTemplate.getInterceptors(), empty());

        final OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) restTemplate;
        assertThat(oAuth2RestTemplate.getOAuth2ClientContext(), instanceOf(DefaultOAuth2ClientContext.class));
        assertThat(oAuth2RestTemplate.getResource().getAccessTokenUri(), is("http://example.com/oauth2"));
        verify(resourceDetails).setUsername(eq("user1"));
        verify(resourceDetails).setPassword(eq("secret1"));
    }

    @Test
    public void shouldReturnRestTemplateWithBasicAuthentication() throws IOException {
        // Given
        final RestServiceConnectorCreator connectorCreator = new RestServiceConnectorCreator();
        final RestServiceInfo serviceInfo = new RestServiceInfo("id", RESTS_BASE_URI);
        serviceInfo.setUserName("user2");
        serviceInfo.setPassword("secret2");

        // When
        final RestTemplate restTemplate = connectorCreator.create(serviceInfo, null);

        // Then
        assertThat(restTemplate, instanceOf(RestTemplate.class));
        assertRestTemplate(restTemplate, "https");
        assertThat(restTemplate.getInterceptors(), hasSize(1));
        assertThat(restTemplate.getInterceptors().get(0), instanceOf(BasicAuthorizationInterceptor.class));
        verify(handler, never()).publish(argThat(LOG_EVENT_MATCHER_WRONG_CONFIG_CLASS));

        // verify the interceptor has user/password behaves as expected
        final BasicAuthorizationInterceptor authorizationInterceptor = (BasicAuthorizationInterceptor) restTemplate.getInterceptors().get(0);
        assertAuthorizationInterceptor(authorizationInterceptor);
    }

    @Test
    public void shouldReturnRestTemplateWithBasicAuthenticationAndWarnAboutUsingRestScheme() throws IOException {
        // Given
        final RestServiceConnectorCreator connectorCreator = new RestServiceConnectorCreator();
        final RestServiceInfo serviceInfo = new RestServiceInfo("id", REST_BASE_URI);
        serviceInfo.setUserName("user2");
        serviceInfo.setPassword("secret2");

        // When
        final RestTemplate restTemplate = connectorCreator.create(serviceInfo, null);

        // Then
        assertThat(restTemplate, instanceOf(RestTemplate.class));
        assertRestTemplate(restTemplate, "http");
        assertThat(restTemplate.getInterceptors(), hasSize(1));
        assertThat(restTemplate.getInterceptors().get(0), instanceOf(BasicAuthorizationInterceptor.class));
        verify(handler, never()).publish(argThat(LOG_EVENT_MATCHER_WRONG_CONFIG_CLASS));
        verify(handler, times(1)).publish(argThat(LOG_EVENT_MATCHER_REST_WITH_BASIC_AUTH));

        // verify the interceptor has user/password behaves as expected
        final BasicAuthorizationInterceptor authorizationInterceptor = (BasicAuthorizationInterceptor) restTemplate.getInterceptors().get(0);
        assertAuthorizationInterceptor(authorizationInterceptor);
    }

    @Test
    public void shouldLoadService() {
        final ServiceLoader<ServiceConnectorCreator> serviceConnectorCreators = ServiceLoader.load(ServiceConnectorCreator.class);

        assertThat(serviceConnectorCreators, hasItem(instanceOf(RestServiceConnectorCreator.class)));
    }

    private void assertAuthorizationInterceptor(BasicAuthorizationInterceptor authorizationInterceptor) throws IOException {
        final HttpHeaders httpHeaders = mock(HttpHeaders.class);
        final HttpRequest httpRequest = mock(HttpRequest.class);
        final ClientHttpRequestExecution clientHttpRequestExecution = mock(ClientHttpRequestExecution.class);
        when(httpRequest.getHeaders()).thenReturn(httpHeaders);

        authorizationInterceptor.intercept(httpRequest, new byte[0], clientHttpRequestExecution);

        verify(httpHeaders).add("Authorization", "Basic dXNlcjI6c2VjcmV0Mg==");
    }

    private void assertRestTemplate(final RestTemplate restTemplate, final String expectedScheme) {
        assertThat(restTemplate.getUriTemplateHandler().expand(PATH).getHost(), is(HOST));
        assertThat(restTemplate.getUriTemplateHandler().expand(PATH).getScheme(), is(expectedScheme));
        assertThat(restTemplate.getUriTemplateHandler().expand(PATH).getPort(), is(PORT));
        assertThat(restTemplate.getUriTemplateHandler().expand(PATH).getPath(), is(PATH));
    }
}
