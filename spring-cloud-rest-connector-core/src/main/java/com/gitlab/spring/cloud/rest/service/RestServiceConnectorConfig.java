package com.gitlab.spring.cloud.rest.service;

/*
 * #%L
 * spring-cloud-rest-connector-core
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.springframework.cloud.service.ServiceConnectorConfig;
import org.springframework.security.oauth2.client.OAuth2ClientContext;

/**
 * Allows setting connection parameters in the {@link org.springframework.web.client.RestTemplate}
 *
 * @author Rolf Aden
 */
public class RestServiceConnectorConfig implements ServiceConnectorConfig {

    private final Integer connectTimeout;
    private final Integer readTimeout;
    private final OAuth2ClientContext oAuth2ClientContext;

    RestServiceConnectorConfig(final Integer connectTimeout, final Integer readTimeout, final OAuth2ClientContext oAuth2ClientContext) {
        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
        this.oAuth2ClientContext = oAuth2ClientContext;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public OAuth2ClientContext getOAuth2ClientContext() {
        return oAuth2ClientContext;
    }

}
