package com.gitlab.spring.cloud.rest.service;

/*
 * #%L
 * spring-cloud-rest-connector-core
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cloud.service.AbstractServiceConnectorCreator;
import org.springframework.cloud.service.ServiceConnectorConfig;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * {@link org.springframework.cloud.service.ServiceConnectorCreator} for Rest Services
 *
 * @author Rolf Aden
 */
public class RestServiceConnectorCreator extends AbstractServiceConnectorCreator<RestTemplate, RestServiceInfo> {

    private static final Log LOG = LogFactory.getLog(RestServiceConnectorCreator.class);

    @Override
    public RestTemplate create(final RestServiceInfo serviceInfo, final ServiceConnectorConfig serviceConnectorConfig) {
        final RestServiceConnectorConfig restServiceConnectorConfig = restServiceConnectorConfig(serviceConnectorConfig);
        RestTemplate restTemplate;

        if (hasOAuth2ConfigurationParameters(serviceInfo)) {
            final ResourceOwnerPasswordResourceDetails resourceDetails = newResourceOwnerPasswordResourceDetails();
            resourceDetails.setAccessTokenUri(serviceInfo.getAccessTokenUri());
            resourceDetails.setClientId(serviceInfo.getClientId());
            resourceDetails.setClientSecret(serviceInfo.getClientSecret());
            resourceDetails.setScope(serviceInfo.getScopes());
            resourceDetails.setUsername(serviceInfo.getUserName());
            resourceDetails.setPassword(serviceInfo.getPassword());

            restTemplate = new OAuth2RestTemplate(resourceDetails, newOAuth2ClientContext(restServiceConnectorConfig));

        } else {
            restTemplate = new RestTemplate();
            if (hasBasicAuthConfigurationParameters(serviceInfo)) {
                if ("rest".equals(serviceInfo.getScheme())) {
                    LOG.warn("You're using 'rest' scheme for basic authentication, better use 'rests'");
                }

                final BasicAuthorizationInterceptor authorizationInterceptor =
                        new BasicAuthorizationInterceptor(serviceInfo.getUserName(), serviceInfo.getPassword());
                restTemplate.getInterceptors().add(authorizationInterceptor);
            }
        }

        configureTimeouts(restTemplate, restServiceConnectorConfig);

        final String rootUri = UriComponentsBuilder.fromUriString(serviceInfo.getUri())
                .scheme(scheme(serviceInfo.getScheme())).build().toUriString();


        RootUriTemplateHandler handler = new RootUriTemplateHandler(rootUri,
                restTemplate.getUriTemplateHandler());
        restTemplate.setUriTemplateHandler(handler);

        RootUriTemplateHandler.addTo(restTemplate, rootUri);

        logInfo(serviceInfo, restTemplate);

        return restTemplate;
    }

    // VisibleForTesting
    ResourceOwnerPasswordResourceDetails newResourceOwnerPasswordResourceDetails() {
        return new ResourceOwnerPasswordResourceDetails();
    }

    private void logInfo(final RestServiceInfo serviceInfo, final RestTemplate restTemplate) {
        if (!LOG.isInfoEnabled()) {
            return;
        }

        final StringBuilder info = new StringBuilder();
        info.append("Rest Service '").append(serviceInfo.getId()).append("' => ");
        if (restTemplate instanceof OAuth2RestTemplate) {
            final OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) restTemplate;
            final OAuth2ProtectedResourceDetails resourceDetails = oAuth2RestTemplate.getResource();
            info.append("OAuth2RestTemplate {");
            info.append("rootUri: ").append(restTemplate.getUriTemplateHandler().expand("/"));
            info.append(", accessTokenUri: ").append(resourceDetails.getAccessTokenUri());
            info.append("}");
        } else {
            info.append("RestTemplate {");
            info.append("rootUri: ").append(restTemplate.getUriTemplateHandler().expand(""));
            info.append("}");
        }

        LOG.info(info.toString());
    }

    private void configureTimeouts(final RestTemplate restTemplate, final RestServiceConnectorConfig restServiceConnectorConfig) {
        if (restServiceConnectorConfig != null
                && (restServiceConnectorConfig.getConnectTimeout() != null || restServiceConnectorConfig.getReadTimeout() != null)) {
            final SimpleClientHttpRequestFactory requestFactory = newSimpleClientHttpRequestFactory();
            if (restServiceConnectorConfig.getConnectTimeout() != null) {
                requestFactory.setConnectTimeout(restServiceConnectorConfig.getConnectTimeout());
            }
            if (restServiceConnectorConfig.getReadTimeout() != null) {
                requestFactory.setReadTimeout(restServiceConnectorConfig.getReadTimeout());
            }
            restTemplate.setRequestFactory(requestFactory);
        }
    }

    // VisibleForTesting
    SimpleClientHttpRequestFactory newSimpleClientHttpRequestFactory() {
        return new SimpleClientHttpRequestFactory();
    }

    private RestServiceConnectorConfig restServiceConnectorConfig(final ServiceConnectorConfig serviceConnectorConfig) {
        RestServiceConnectorConfig restServiceConnectorConfig = null;
        if (serviceConnectorConfig != null) {
            if (serviceConnectorConfig instanceof RestServiceConnectorConfig) {
                restServiceConnectorConfig = (RestServiceConnectorConfig) serviceConnectorConfig;
            } else {
                LOG.warn(String.format("Use %s for configuring the rest service connection",
                        RestServiceConnectorConfig.class));
            }
        }
        return restServiceConnectorConfig;
    }

    private String scheme(final String scheme) {
        switch (scheme) {
            case "rest":
                return "http";
            case "rests":
                return "https";
            default:
                return "https";
        }
    }

    private boolean hasOAuth2ConfigurationParameters(final RestServiceInfo serviceInfo) {
        return serviceInfo.getAccessTokenUri() != null;
    }

    private boolean hasBasicAuthConfigurationParameters(final RestServiceInfo serviceInfo) {
        return serviceInfo.getAccessTokenUri() == null && serviceInfo.getUserName() != null;
    }

    private OAuth2ClientContext newOAuth2ClientContext(final RestServiceConnectorConfig restServiceConnectorConfig) {
        if (restServiceConnectorConfig != null && restServiceConnectorConfig.getOAuth2ClientContext() != null) {
            return restServiceConnectorConfig.getOAuth2ClientContext();
        } else {
            return new DefaultOAuth2ClientContext(new DefaultAccessTokenRequest());
        }
    }

}
