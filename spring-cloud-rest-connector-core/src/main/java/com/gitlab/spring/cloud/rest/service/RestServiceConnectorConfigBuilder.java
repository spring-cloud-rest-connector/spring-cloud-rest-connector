package com.gitlab.spring.cloud.rest.service;

/*
 * #%L
 * spring-cloud-rest-connector-core
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.springframework.security.oauth2.client.OAuth2ClientContext;

/**
 * Builder for {@link RestServiceConnectorConfig}.
 *
 * @author Rolf Aden
 */
public class RestServiceConnectorConfigBuilder {

    private RestServiceConnectorConfigBuilder() {
    }

    /**
     * Return a new {@link RestServiceConnectorConfigBuilder} for {@link RestServiceConnectorConfig}
     *
     * @return the a new builder
     */
    public static RestServiceConnectorConfigBuilder newRestServiceConnectorConfig() {
        return new RestServiceConnectorConfigBuilder();
    }

    private Integer connectTimeout;
    private Integer readTimeout;
    private OAuth2ClientContext oAuth2ClientContext;

    /**
     * Configures the connectTimeout of the {@link org.springframework.http.client.ClientHttpRequestFactory} used
     * in the {@link org.springframework.web.client.RestTemplate}
     *
     * @param connectTimeout in ms
     * @return the current builder
     */
    public RestServiceConnectorConfigBuilder withConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    /**
     * Configures the readTimeout of the {@link org.springframework.http.client.ClientHttpRequestFactory} used
     * in the {@link org.springframework.web.client.RestTemplate}
     *
     * @param readTimeout in ms
     * @return the current builder
     */
    public RestServiceConnectorConfigBuilder withReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    /**
     * Lets you set the {@link OAuth2ClientContext} in the {@link org.springframework.security.oauth2.client.OAuth2RestTemplate}
     *
     * @param oAuth2ClientContext to use
     * @return the current builder
     */
    public RestServiceConnectorConfigBuilder withOAuth2ClientContext(OAuth2ClientContext oAuth2ClientContext) {
        this.oAuth2ClientContext = oAuth2ClientContext;
        return this;
    }

    /**
     * Build the {@link RestServiceConnectorConfig}.
     *
     * @return the new RestServiceConnectorConfig
     */
    public RestServiceConnectorConfig build() {
        return new RestServiceConnectorConfig(this.connectTimeout, this.readTimeout, this.oAuth2ClientContext);
    }
}
