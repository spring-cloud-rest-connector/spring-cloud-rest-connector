package com.gitlab.spring.cloud.rest.service;

/*
 * #%L
 * spring-cloud-rest-connector-core
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.springframework.cloud.service.UriBasedServiceInfo;

import java.util.List;

/**
 * Configuration information for rest services. It contains also config for accessing oauth2 protected resources
 *
 * @author Rolf Aden
 */
public class RestServiceInfo extends UriBasedServiceInfo {

    private String accessTokenUri;
    private String clientId;
    private String clientSecret;
    private List<String> scopes;
    private String username;
    private String password;

    public RestServiceInfo(String id, String uriString) {
        super(id, uriString);
    }

    public String getAccessTokenUri() {
        return accessTokenUri;
    }

    public void setAccessTokenUri(String accessTokenUri) {
        this.accessTokenUri = accessTokenUri;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    @Override
    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
