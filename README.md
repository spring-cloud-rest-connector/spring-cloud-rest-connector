[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.gitlab.spring-cloud-rest-connector%3Aspring-cloud-rest-connector&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.gitlab.spring-cloud-rest-connector%3Aspring-cloud-rest-connector)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.gitlab.spring-cloud-rest-connector%3Aspring-cloud-rest-connector&metric=coverage)](https://sonarcloud.io/dashboard?id=com.gitlab.spring-cloud-rest-connector%3Aspring-cloud-rest-connector)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.gitlab.spring-cloud-rest-connector/spring-cloud-rest-connector/badge.svg?style=plastic)](https://maven-badges.herokuapp.com/maven-central/com.gitlab.spring-cloud-rest-connector/spring-cloud-rest-connector)

# Spring Cloud Rest Connector

Allows you to configure a rest service using [spring cloud connector](http://cloud.spring.io/spring-cloud-connectors/) for different environments.
Currenly _local config_ and _cloud foundry_ are supported. The connector returns a RestTemplate you can use to call your service.

## How to use it

### Maven Dependencies

```xml
    <properties>
        <rest.connector.version>see maven central badge at the top of the README.md</rest.connector.version>
    <properties>
...
    <dependency>
        <groupId>com.gitlab.spring-cloud-rest-connector</groupId>
        <artifactId>spring-cloud-rest-connector-core</artifactId>
        <version>${rest.connector.version}</version>
    </dependency>
    <dependency>
        <groupId>com.gitlab.spring-cloud-rest-connector</groupId>
        <artifactId>spring-cloud-rest-cloudfoundry-connector</artifactId>
        <version>${rest.connector.version}</version>
    </dependency>
    <dependency>
        <groupId>com.gitlab.spring-cloud-rest-connector</groupId>
        <artifactId>spring-cloud-rest-localconfig-connector</artifactId>
        <version>${rest.connector.version}</version>
    </dependency>
```

### Instantiate the RestTemplate  

```java
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import com.gitlab.spring.cloud.rest.service.RestServiceConnectorConfigBuilder;

@Configuration
public class Config extends AbstractCloudConfig {

    @Bean
    public RestTemplate myRestService() {
        final RestServiceConnectorConfig connectorConfig = RestServiceConnectorConfigBuilder
                .newRestServiceConnectorConfig()
                .withConnectTimeout(1000).withReadTimeout(2000)
                .build();

        return cloud().getServiceConnector("my-rest-service", RestTemplate.class, connectorConfig);
    }

}
```

## Configuration

The spring cloud rest connector sees service urls with the scheme _rest_ or _rests_. 
_Rest_ will be translated into a rest service url with the scheme _http_ and _rests_ translates to _http_.

The following parameters can be configured.

| Parameter | Description |
| --------- | ----------- |
| uri | Root uri of the rest service but with the schema _rest_ or _rests_ insted of _http_ or _https_ |
| accessTokenUri | http://localhost:8080/oauth |
| clientId | client id of the application |
| clientSecret | secret of the application |
| scopes | the requested oauth scopes |
| username | user the service is acting on behave |
| password | the users password |

If _accessTokenUri_ is given then an _OAuth2RestTemplate_ using the _ResourceOwnerPasswordResourceDetails_ 
filled with the parameter listed above will be used.

If no _accessTokenUri_ is present but the _username_ then the _RestTemplate_ will be configured with basic 
authentication using _username_ and _password_. 

### Local Config

Configure the rest service in the _spring.cloud.propertiesFile_.
Please find [here](http://cloud.spring.io/spring-cloud-connectors/spring-cloud-connectors.html#_quick_start) the instructions.
 
```.properties
spring.cloud.my-rest-service: rest://localhost:8080
```

With the above configuration you'll get a _RestTemplate_ with the _rootUri_ _http://localhost:8080_.

All parameters apart from the _uri_ can be configured in a file. This file can be set with the system property
_spring.rest.connector.localconfig_ and defaults to _localconfig/rest-services.yml_. See an example below.

```
my-rest-service:
      accessTokenUri: http://localhost:8080/oauth
      clientId: my-client-id
      clientSecret: my-client-secret
      scopes:
        - read
        - write
      username: user
      password: password
```

### Cloud Foundry

See the [cloud foundry](https://docs.cloudfoundry.org/devguide/services/user-provided.html) documentation for creating user provided services.

```
cf cups my-rest-service -p "{\"uri\": \"http://host.example.com\"}"
cf bind-service my-app my-rest-service
```

All parameters apart from the _uri_ can be added to the _credentials_. The scopes need to be separated by commas.