package com.gitlab.spring.cloud.rest.localconfig;

/*
 * #%L
 * spring-cloud-rest-localconfig-connector
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.gitlab.spring.cloud.rest.service.RestServiceInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.YamlMapFactoryBean;
import org.springframework.cloud.localconfig.LocalConfigServiceInfoCreator;
import org.springframework.core.io.FileSystemResource;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

/**
 * This is the local config version of the {@link org.springframework.cloud.ServiceInfoCreator}.
 * <p>
 * {@inheritDoc}
 *
 * @author Rolf Aden
 */
public class RestServiceInfoCreator extends LocalConfigServiceInfoCreator<RestServiceInfo> {

    private static final Log LOG = LogFactory.getLog(RestServiceInfoCreator.class);

    private static final String CONFIG_FILE_PROPERTY = "spring.rest.connector.localconfig";
    private static final String DEFAULT_CONFIG_FILE = "localconfig/rest-services.yml";
    public static final String INFO_MESSAGE =
            "If you want to configure more properties than the uri you can create a file\n" +
                    "with the system property %s pointing to it (default file name %s)\n" +
                    "and with a content like this:\n" +
                    "my-rest-service:\n" +
                    "      accessTokenUri: http://localhost:8080/oauth\n" +
                    "      clientId: my-client-id\n" +
                    "      clientSecret: my-client-secret\n" +
                    "      scopes:\n" +
                    "        - read\n" +
                    "        - write\n" +
                    "      username: user\n" +
                    "      password: password";
    private final String configFile;

    public RestServiceInfoCreator() {
        this(System.getProperty(CONFIG_FILE_PROPERTY, DEFAULT_CONFIG_FILE));
    }

    RestServiceInfoCreator(String configFile) {
        super("rest", "rests");
        this.configFile = configFile;
    }

    @Override
    public RestServiceInfo createServiceInfo(String id, String uri) {
        final RestServiceInfo restServiceInfo = new RestServiceInfo(id, uri);

        final Map<String, Object> restServiceConfig = readRestServiceConfig();
        if (restServiceConfig != null) {
            final Map<String, Object> config = (Map<String, Object>) restServiceConfig.get(id);
            if (config != null) {
                configure(restServiceInfo, config);
            } else {
                LOG.info(String.format(INFO_MESSAGE, CONFIG_FILE_PROPERTY, DEFAULT_CONFIG_FILE));
            }
        }

        return restServiceInfo;
    }

    private Map<String, Object> readRestServiceConfig() {
        try {
            final YamlMapFactoryBean yamlMapFactoryBean = new YamlMapFactoryBean();
            yamlMapFactoryBean.setResources(new FileSystemResource(configFile));
            return yamlMapFactoryBean.getObject();
        } catch (IllegalStateException ise) {
            if (ise.getCause() instanceof FileNotFoundException) {
                LOG.info(String.format(INFO_MESSAGE, CONFIG_FILE_PROPERTY, DEFAULT_CONFIG_FILE) + "\n" + ise.getCause());
                return null;
            }
            throw ise;
        }
    }

    private void configure(final RestServiceInfo restServiceInfo, final Map<String, Object> config) {
        for (Map.Entry<String, Object> entry : config.entrySet()) {
            switch (entry.getKey()) {
                case "accessTokenUri":
                    restServiceInfo.setAccessTokenUri(getValueAsString(entry));
                    break;
                case "clientId":
                    restServiceInfo.setClientId(getValueAsString(entry));
                    break;
                case "clientSecret":
                    restServiceInfo.setClientSecret(getValueAsString(entry));
                    break;
                case "scopes":
                    restServiceInfo.setScopes(getValueAsList(entry));
                    break;
                case "username":
                    restServiceInfo.setUserName(getValueAsString(entry));
                    break;
                case "password":
                    restServiceInfo.setPassword(getValueAsString(entry));
                    break;
                default:
                    LOG.warn(String.format("Unknown configuration parameter %s in config %s", entry.getKey(), this.configFile));
            }
        }
    }

    private String getValueAsString(final Map.Entry<String, Object> entry) {
        if (entry.getValue() instanceof String) {
            return (String) entry.getValue();
        } else if (entry.getValue() instanceof Integer) { // we want strings but also except Integers
            return entry.getValue().toString();
        } else {
            LOG.warn(String.format("The value of %s must be a string but is %s", entry.getKey(), entry.getValue()));
            return null;
        }
    }

    private List<String> getValueAsList(final Map.Entry<String, Object> entry) {
        if (entry.getValue() instanceof List &&
                ((List) entry.getValue()).stream().allMatch(value -> value instanceof String)) {
            return (List<String>) entry.getValue();
        } else {
            LOG.warn(String.format("The value of %s must be an array of strings but is %s", entry.getKey(), entry.getValue()));
            return emptyList();
        }
    }

}
