package com.gitlab.spring.cloud.rest.localconfig;

/*
 * #%L
 * spring-cloud-rest-localconfig-connector
 * %%
 * Copyright (C) 2017 Rolf Aden
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.gitlab.spring.cloud.rest.service.RestServiceConnectorCreator;
import com.gitlab.spring.cloud.rest.service.RestServiceInfo;
import org.hamcrest.collection.IsArrayContainingInAnyOrder;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.cloud.localconfig.LocalConfigServiceInfoCreator;

import java.awt.*;
import java.util.ServiceLoader;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

/**
 * Tests the {@link RestServiceConnectorCreator}
 *
 * @author Rolf Aden
 */
@RunWith(MockitoJUnitRunner.class)
public class RestServiceInfoCreatorTest {

    public static final String START_OF_INFO_MESSAGE = "If you want to configure more properties";
    public static final LogEventMatcher INFO_LOG_MATCHER = new LogEventMatcher(Level.INFO, START_OF_INFO_MESSAGE);

    @Spy
    private Handler handler;

    @BeforeClass
    public static void setConfigSystemProperty() {
        System.setProperty("spring.rest.connector.localconfig", "localconfig/rest-services-test-2.yml");
    }

    @Before
    public void setupLogger() {
        final Logger logger = Logger.getLogger(RestServiceInfoCreator.class.getName());
        handler.setLevel(Level.ALL);
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.ALL);
    }

    @After
    public void cleanupLogger() {
        final Logger logger = Logger.getLogger(RestServiceInfoCreator.class.getName());
        logger.removeHandler(handler);
    }

    @Test
    public void shouldLogMissingRestServiceConfigYAML() {
        // Given
        final RestServiceInfoCreator serviceInfoCreator = new RestServiceInfoCreator("does not exist");

        // When
        final RestServiceInfo serviceInfo = serviceInfoCreator.createServiceInfo("123", "rest://host.example.com");

        // Then
        assertThat(serviceInfo.getId(), is("123"));
        assertThat(serviceInfo.getUri(), is("rest://host.example.com"));

        verify(handler, times(1)).publish(argThat(INFO_LOG_MATCHER));
    }

    @Test
    public void shouldLogMissingServiceEntryInRestServiceConfigYAML() {
        // Given
        final RestServiceInfoCreator serviceInfoCreator = new RestServiceInfoCreator("localconfig/rest-services-test.yml");

        // When
        final RestServiceInfo serviceInfo = serviceInfoCreator.createServiceInfo("123", "rest://host2.example.com");

        // Then
        assertThat(serviceInfo.getId(), is("123"));
        assertThat(serviceInfo.getUri(), is("rest://host2.example.com"));
        verify(handler, times(1)).publish(argThat(INFO_LOG_MATCHER));
    }


    @Test
    public void shouldReadValuesFromRestServiceConfigYAML() {
        // Given
        final RestServiceInfoCreator serviceInfoCreator = new RestServiceInfoCreator("localconfig/rest-services-test.yml");

        // When
        final RestServiceInfo serviceInfo = serviceInfoCreator.createServiceInfo("my-rest-service", "rest://host.example.com");

        // Then
        assertThat(serviceInfo.getId(), is("my-rest-service"));
        assertThat(serviceInfo.getAccessTokenUri(), is("http://localhost:8080/oauth"));
        assertThat(serviceInfo.getClientId(), is("my-client-id"));
        assertThat(serviceInfo.getClientSecret(), is("my-client-secret"));
        assertThat(serviceInfo.getScopes().toArray(), IsArrayContainingInAnyOrder.arrayContainingInAnyOrder("read", "write"));
        assertThat(serviceInfo.getUserName(), is("user"));
        assertThat(serviceInfo.getPassword(), is("password"));

        verify(handler, never()).publish(argThat(INFO_LOG_MATCHER));
    }

    @Test
    public void shouldReadValuesFromRestServiceConfigYAMLIdenfiedBySystemProperty() {
        // Given
        final RestServiceInfoCreator serviceInfoCreator = new RestServiceInfoCreator();

        // When
        final RestServiceInfo serviceInfo = serviceInfoCreator.createServiceInfo("my-rest-service", "rest://host.example.com");

        // Then
        assertThat(serviceInfo.getId(), is("my-rest-service"));
        assertThat(serviceInfo.getAccessTokenUri(), is("http://localhost:8080/oauth2"));
        assertThat(serviceInfo.getClientId(), is("my-client-id-2"));
        assertThat(serviceInfo.getClientSecret(), is("my-client-secret-2"));
        assertThat(serviceInfo.getScopes().toArray(), IsArrayContainingInAnyOrder.arrayContainingInAnyOrder("scope1", "scope2"));
        assertThat(serviceInfo.getUserName(), is("user-2"));
        assertThat(serviceInfo.getPassword(), is("password-2"));

        verify(handler, never()).publish(argThat(INFO_LOG_MATCHER));
    }

    @Test
    public void shouldLogUnknownConfigParameters() {
        // Given
        final RestServiceInfoCreator serviceInfoCreator = new RestServiceInfoCreator("localconfig/rest-services-test-with-wrong-parameters.yml");

        // When
        final RestServiceInfo serviceInfo = serviceInfoCreator.createServiceInfo("my-rest-service", "rest://host.example.com");

        // Then
        assertThat(serviceInfo.getId(), is("my-rest-service"));
        assertThat(serviceInfo.getAccessTokenUri(), is("http://localhost:8080/oauth"));
        assertThat(serviceInfo.getClientId(), is("my-client-id"));
        assertThat(serviceInfo.getClientSecret(), is("my-client-secret"));
        assertThat(serviceInfo.getScopes(), empty());
        assertThat(serviceInfo.getUserName(), is(nullValue()));
        assertThat(serviceInfo.getPassword(), is("123"));

        verify(handler, never()).publish(argThat(INFO_LOG_MATCHER));
        verify(handler, times(1)).publish(argThat(new LogEventMatcher(Level.WARNING, "Unknown configuration parameter")));
        verify(handler, times(1)).publish(argThat(new LogEventMatcher(Level.WARNING, "The value of username must be a string but is [user1, user2]")));
        verify(handler, times(1)).publish(argThat(new LogEventMatcher(Level.WARNING, "The value of scopes must be an array of strings but is scope1")));
    }


    @Test
    public void shouldLoadService() {
        // Given

        // When
        final ServiceLoader<LocalConfigServiceInfoCreator> serviceInfoCreators = ServiceLoader.load(LocalConfigServiceInfoCreator.class);

        // Then
        assertThat(serviceInfoCreators, hasItem(instanceOf(RestServiceInfoCreator.class)));
    }

}
